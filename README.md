# Wild Shape API

A Roll20 API for D&D 5th Edition to make the Druid's Wild Shape ability much faster and easier to manage.

## Archived

This project is to be archived in favour of the pre-existing WildShape API ([Roll20 Forum](https://app.roll20.net/forum/post/8856337/script-wildshape-easy-shapeshift-for-your-pcs-and-npcs/?pagenum=1) | [GitHub](https://github.com//ocangelo/roll20/tree/master/WildShape)).
