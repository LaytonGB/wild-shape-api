const WildShapeAPI = (function () {
    function handleInput(msg: ChatEventData) {
        if (msg.type === 'api') {
            const parts = msg.content.split(/ +/);
            if (parts[0] === '!ws') {
                // do thing
            }
        }
    }

    function startup() {
        registerEventHandlers();
    }

    function registerEventHandlers() {
        on('chat:message', handleInput);
    }

    return {
        Startup: startup,
    };
})();

on('ready', () => {
    WildShapeAPI.Startup();
});
